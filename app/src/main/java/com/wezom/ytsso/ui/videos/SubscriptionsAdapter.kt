package com.wezom.ytsso.ui.videos

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.wezom.ytsso.databinding.ItemSubBinding
import com.wezom.ytsso.network.models.Subscription

class SubscriptionsAdapter : RecyclerView.Adapter<SubscriptionsAdapter.SubscriptionHolder>() {

    private val items: ArrayList<Subscription> = arrayListOf()

    class SubscriptionHolder(private val binding: ItemSubBinding) : RecyclerView.ViewHolder(binding.root) {

        fun put(sub: Subscription) {
            setPic(sub.snippet.thumbnails.high.url)
            binding.title.text = sub.snippet.title
            binding.subtitle.text = sub.snippet.description
        }

        private fun setPic(url: String) {
            Picasso.get().load(url).into(binding.pic)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SubscriptionHolder {
        val binding = ItemSubBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SubscriptionHolder(binding)
    }

    override fun onBindViewHolder(holder: SubscriptionHolder, position: Int) {
        val sub = items[position]
        holder.put(sub)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun update(subs: ArrayList<Subscription>) {
        items.clear()
        items.addAll(subs)
        notifyDataSetChanged()
    }
}