package com.wezom.ytsso.ui.auth

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.wezom.ytsso.databinding.AuthBinding
import com.wezom.ytsso.ui.base.BaseFragment

class AuthFragment : BaseFragment() {

    private lateinit var binding: AuthBinding
    private lateinit var viewModel: AuthViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = getViewModel()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = AuthBinding.inflate(inflater, container, false)
        binding.vm = viewModel
        return binding.root
    }
}