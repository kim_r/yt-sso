package com.wezom.ytsso.ui.auth

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import androidx.core.net.toUri
import com.wezom.ytsso.common.*
import com.wezom.ytsso.ui.base.BaseViewModel
import net.openid.appauth.AuthorizationRequest
import net.openid.appauth.AuthorizationService
import net.openid.appauth.AuthorizationServiceConfiguration
import javax.inject.Inject

class AuthViewModel @Inject constructor(
    private val appContext: Context,
    private val authService: AuthorizationService
) : BaseViewModel() {

    fun auth() {
        // create request
        val config = AuthorizationServiceConfiguration(AUTH_ENDPOINT.toUri(), TOKEN_ENDPOINT.toUri())
        val authRequest = AuthorizationRequest.Builder(
            config,
            OAUTH_CLIENT_ID,
            AuthorizationRequest.RESPONSE_TYPE_CODE,
            REDIRECT.toUri()
        ).setScopes(SCOPES).build()
        // perform request
        val intent = Intent(ACTION_HANDLE_AUTH)
        val pending = PendingIntent.getActivity(appContext, 0, intent, 0)
        authService.performAuthorizationRequest(authRequest, pending)
    }
}