package com.wezom.ytsso.ui.videos

import android.os.Bundle
import android.view.*
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.wezom.ytsso.R
import com.wezom.ytsso.databinding.VideosBinding
import com.wezom.ytsso.network.models.Subscription
import com.wezom.ytsso.ui.base.BaseFragment

/**
 * Regardless of the fact that there's a word "videos" in the name of this fragment,
 * here we show user's subscriptions from YouTube.
 */
class VideosFragment : BaseFragment() {

    private lateinit var binding: VideosBinding
    private lateinit var viewModel: VideosViewModel

    private val adapter = SubscriptionsAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        viewModel = getViewModel()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = VideosBinding.inflate(inflater, container, false)
        binding.vm = viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.recycler.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = this@VideosFragment.adapter
        }
        subscribeToLiveData()
        viewModel.getSubscriptions()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.menu_videos_fragment, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.menu_item_logout -> {
                viewModel.logout()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun subscribeToLiveData() {
        viewModel.subscriptionsLiveData.observe(viewLifecycleOwner, Observer { subscriptions ->
            subscriptions ?: return@Observer
            adapter.update(subscriptions as ArrayList<Subscription>)
        })
    }
}