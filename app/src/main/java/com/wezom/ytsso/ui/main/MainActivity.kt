package com.wezom.ytsso.ui.main

import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.navigation.Navigation
import com.wezom.ytsso.R
import com.wezom.ytsso.common.ACTION_HANDLE_AUTH
import com.wezom.ytsso.databinding.MainBinding
import com.wezom.ytsso.navigation.NavRouter
import com.wezom.ytsso.ui.base.BaseActivity
import javax.inject.Inject

class MainActivity : BaseActivity() {

    companion object {
        private const val USED_INTENT = "USED_INTENT"
    }

    @Inject lateinit var router: NavRouter

    private lateinit var binding: MainBinding
    private lateinit var viewModel: MainActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        viewModel = getViewModel()
        val navController = Navigation.findNavController(this, R.id.fragment_container)
        router.setController(navController)
        viewModel.checkToken()
    }

    override fun onStart() {
        super.onStart()
        checkIntent(intent)
    }

    override fun onNewIntent(intent: Intent?) {
        checkIntent(intent)
    }

    private fun checkIntent(intent: Intent?) {
        intent ?: return
        if (intent.action != ACTION_HANDLE_AUTH) return
        if (!intent.hasExtra(USED_INTENT)) {
            viewModel.handleAuthResponse(intent)
            intent.putExtra(USED_INTENT, true)
        }
    }
}
