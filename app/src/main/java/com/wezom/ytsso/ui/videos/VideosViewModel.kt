package com.wezom.ytsso.ui.videos

import android.content.SharedPreferences
import androidx.core.content.edit
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.MutableLiveData
import com.wezom.ytsso.R
import com.wezom.ytsso.common.PrefKeys
import com.wezom.ytsso.navigation.NavRouter
import com.wezom.ytsso.network.ApiManager
import com.wezom.ytsso.network.models.Subscription
import com.wezom.ytsso.ui.base.BaseViewModel
import io.reactivex.rxkotlin.plusAssign
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class VideosViewModel @Inject constructor(
    private val api: ApiManager,
    private val prefs: SharedPreferences,
    private val router: NavRouter
) : BaseViewModel() {

    var isLoading = ObservableBoolean()

    val subscriptionsLiveData = MutableLiveData<List<Subscription>>()

    fun getSubscriptions() {
        isLoading.set(true)
        val token = prefs.getString(PrefKeys.ACCESS_TOKEN, null)
        val refreshToken = prefs.getString(PrefKeys.REFRESH_TOKEN, null)
        if (token == null || refreshToken == null) {
            logout()
            return
        }
        // check if now is time to refresh token
        val exp = prefs.getLong(PrefKeys.TOKEN_EXP_TIME, 0)
        if (exp < System.currentTimeMillis()) {
            refreshTokenAndFetchSubs(exp, refreshToken)
        } else {
            fetchSubs(token)
        }
    }

    fun logout() {
        prefs.edit { putString(PrefKeys.ACCESS_TOKEN, null) }
        router.setRootDestination(R.id.auth_fragment)
    }

    private fun refreshTokenAndFetchSubs(oldExpTime: Long, rt: String) {
        disposables += api.refreshToken(rt).subscribe(
            { response ->
                prefs.edit {
                    putString(PrefKeys.ACCESS_TOKEN, response.accessToken)
                    putLong(PrefKeys.TOKEN_EXP_TIME, oldExpTime + TimeUnit.SECONDS.toMillis(response.expiresIn))
                }
                fetchSubs(response.accessToken)
            },
            Timber::e
        )
    }

    private fun fetchSubs(token: String) {
        disposables += api.getSubscriptions(token).subscribe(
            { response ->
                isLoading.set(false)
                subscriptionsLiveData.postValue(response.items)
            },
            Timber::e
        )
    }
}