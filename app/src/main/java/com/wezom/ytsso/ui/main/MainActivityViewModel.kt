package com.wezom.ytsso.ui.main

import android.content.Intent
import android.content.SharedPreferences
import androidx.core.content.edit
import com.wezom.ytsso.R
import com.wezom.ytsso.common.PrefKeys
import com.wezom.ytsso.navigation.NavRouter
import com.wezom.ytsso.ui.base.BaseViewModel
import net.openid.appauth.AuthorizationResponse
import net.openid.appauth.AuthorizationService
import timber.log.Timber
import javax.inject.Inject

class MainActivityViewModel @Inject constructor(
    private val authService: AuthorizationService,
    private val prefs: SharedPreferences,
    private val router: NavRouter
) : BaseViewModel() {

    fun handleAuthResponse(intent: Intent) {
        val response = AuthorizationResponse.fromIntent(intent) ?: return
        authService.performTokenRequest(response.createTokenExchangeRequest()) { tokenResponse, e ->
            if (e != null) {
                Timber.e("something wrong with authorization")
                return@performTokenRequest
            }
            if (tokenResponse != null) {
                Timber.d("token was got ${tokenResponse.toJsonString()}")
                // save token and switch to another screen
                prefs.edit {
                    putString(PrefKeys.ACCESS_TOKEN, tokenResponse.accessToken)
                    putString(PrefKeys.REFRESH_TOKEN, tokenResponse.refreshToken)
                    putLong(PrefKeys.TOKEN_EXP_TIME, tokenResponse.accessTokenExpirationTime!!)
                }
                router.setRootDestination(R.id.videos_fragment)
            }
        }
    }

    fun checkToken() {
        prefs.getString(PrefKeys.ACCESS_TOKEN, null) ?: return
        router.setRootDestination(R.id.videos_fragment)
    }
}