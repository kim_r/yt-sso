package com.wezom.ytsso.navigation

import android.os.Bundle

/**
 *Created by Zorin.A on 20.March.2019.
 */
interface Navigation {
    fun navigateTo(destination: Int)
    fun navigateTo(destination: Int, bundle: Bundle)
    fun navigateUp()
    fun popBackStack(): Boolean
    fun popBackStack(destinationId: Int, isInclusive: Boolean): Boolean
    fun setStartDestination(destination: Int)
    fun setRootDestination(destinationId: Int)
}