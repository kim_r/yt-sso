package com.wezom.ytsso.navigation

import androidx.navigation.NavController

/**
 *Created by Zorin.A on 20.March.2019.
 */
interface NavControllerProvider {
    fun provideNavController(): NavController
}