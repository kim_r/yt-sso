package com.wezom.ytsso.navigation

import javax.inject.Inject
import javax.inject.Singleton

/**
 *Created by Zorin.A on 20.March.2019.
 */
@Singleton
class NavManager @Inject constructor(private val router: NavRouter) : Navigation by router
