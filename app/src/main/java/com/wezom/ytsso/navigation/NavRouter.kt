package com.wezom.ytsso.navigation

import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import javax.inject.Inject
import javax.inject.Singleton

/**
 *Created by Zorin.A on 20.March.2019.
 */
@Singleton
open class NavRouter @Inject constructor() : Navigation {

    private var controller: NavController? = null

    fun setController(controller: NavController) {
        this.controller = controller
    }

    fun removeController() {
        controller = null
    }

    override fun navigateTo(destination: Int) {
        controller?.navigate(destination)
        controller?.popBackStack()
    }

    override fun navigateTo(destination: Int, bundle: Bundle) {
        controller?.navigate(destination, bundle)
    }

    override fun setRootDestination(destinationId: Int) {
        val b = NavOptions.Builder()
        val currentDestinationId = controller?.currentDestination?.id
        b.setPopUpTo(currentDestinationId!!, true)
        controller?.navigate(destinationId, null, b.build())
    }

    override fun navigateUp() {
        controller?.navigateUp()
    }

    override fun popBackStack(): Boolean {
        return controller?.popBackStack()!!
    }

    override fun popBackStack(destinationId: Int, isInclusive: Boolean): Boolean {
        return controller?.popBackStack(destinationId, isInclusive)!!
    }

    override fun setStartDestination(destination: Int) {
        controller?.graph?.startDestination = destination
    }
}