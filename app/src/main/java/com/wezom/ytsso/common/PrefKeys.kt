package com.wezom.ytsso.common

object PrefKeys {
    const val ACCESS_TOKEN = "access_token"
    const val REFRESH_TOKEN = "refresh_token"
    const val TOKEN_EXP_TIME = "token_exp_time"
}