package com.wezom.ytsso.common

const val AUTH_ENDPOINT = "https://accounts.google.com/o/oauth2/v2/auth"
const val TOKEN_ENDPOINT = "https://www.googleapis.com/oauth2/v4/token"

const val OAUTH_CLIENT_ID = "453721705082-dfhmj5gq00gvugflie9nta9a5ch37iff.apps.googleusercontent.com"
const val REDIRECT = "com.wezom.ytsso:/oauth2callback"

const val ACTION_HANDLE_AUTH = "HANDLE_AUTHORIZATION_RESPONSE"

val SCOPES = arrayListOf(
    "https://www.googleapis.com/auth/userinfo.profile",
    "https://www.googleapis.com/auth/youtube.readonly"
)