package com.wezom.ytsso.di

import android.app.Application
import com.wezom.ytsso.App
import com.wezom.ytsso.di.modules.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AndroidSupportInjectionModule::class,
    AppModule::class,
    NetworkModule::class,
    ActivityModule::class,
    FragmentModule::class,
    ViewModelModule::class
])
interface RootComponent : AndroidInjector<App> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun bindApplication(application: Application): Builder
        fun build(): RootComponent
    }

    override fun inject(app: App)
}