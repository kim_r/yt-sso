package com.wezom.ytsso.di.modules

import com.wezom.ytsso.ui.auth.AuthFragment
import com.wezom.ytsso.ui.videos.VideosFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface FragmentModule {

    @ContributesAndroidInjector
    fun contributeAuthFragment(): AuthFragment

    @ContributesAndroidInjector
    fun contributeVideosFragment(): VideosFragment
}