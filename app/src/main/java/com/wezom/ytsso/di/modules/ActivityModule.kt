package com.wezom.ytsso.di.modules

import androidx.appcompat.app.AppCompatActivity
import com.wezom.ytsso.ui.main.MainActivity
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface ActivityModule {

    @Binds
    fun provideMainActivity(activity: MainActivity): AppCompatActivity // what is this?

    @ContributesAndroidInjector(modules = [FragmentModule::class])
    fun contributeMainActivity(): MainActivity
}