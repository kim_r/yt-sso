package com.wezom.ytsso.di.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.wezom.ytsso.di.ViewModelFactory
import com.wezom.ytsso.di.ViewModelKey
import com.wezom.ytsso.ui.auth.AuthViewModel
import com.wezom.ytsso.ui.main.MainActivityViewModel
import com.wezom.ytsso.ui.videos.VideosViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface ViewModelModule {

    @Binds
    fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(MainActivityViewModel::class)
    fun bindsMainActivityViewModel(vm: MainActivityViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AuthViewModel::class)
    fun bindsAuthViewModel(vm: AuthViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(VideosViewModel::class)
    fun bindsVideosViewModel(vm: VideosViewModel): ViewModel
}