package com.wezom.ytsso.network.responses

import com.wezom.ytsso.network.models.Error
import com.wezom.ytsso.network.models.Subscription

data class SubscriptionsResponse(
    val items: List<Subscription>?,
    val error: Error?
)