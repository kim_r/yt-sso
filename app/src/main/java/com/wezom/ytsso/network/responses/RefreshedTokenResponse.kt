package com.wezom.ytsso.network.responses

import com.google.gson.annotations.SerializedName

data class RefreshedTokenResponse(
    @SerializedName("access_token") val accessToken: String,
    @SerializedName("expires_in") val expiresIn: Long,
    @SerializedName("scope") val scope: String,
    @SerializedName("token_type") val tokenType: String,
    @SerializedName("id_token") val idToken: String
)