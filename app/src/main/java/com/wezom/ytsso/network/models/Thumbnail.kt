package com.wezom.ytsso.network.models

data class Thumbnail(
    val url: String
)