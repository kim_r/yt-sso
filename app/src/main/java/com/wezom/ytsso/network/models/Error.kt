package com.wezom.ytsso.network.models

data class Error(
    val code: Int,
    val message: String
)