package com.wezom.ytsso.network

import com.wezom.ytsso.common.OAUTH_CLIENT_ID
import com.wezom.ytsso.common.TOKEN_ENDPOINT
import com.wezom.ytsso.network.responses.RefreshedTokenResponse
import com.wezom.ytsso.network.responses.SubscriptionsResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.Query

interface YoutubeApiService {

    @GET("subscriptions")
    fun getSubscriptions(
        @Header("Authorization") token: String,
        @Query("part") part: String = "snippet",
        @Query("mine") mine: Boolean = true,
        @Query("maxResults") maxResults: Int = 50
    ): Single<SubscriptionsResponse>

    @POST(TOKEN_ENDPOINT)
    fun refreshToken(
        @Query("refresh_token") refreshToken: String,
        @Query("client_id") clientId: String = OAUTH_CLIENT_ID,
        @Query("grant_type") grantType: String = "refresh_token"
    ): Single<RefreshedTokenResponse>
}