package com.wezom.ytsso.network.models

import com.google.gson.annotations.SerializedName

data class SubscriptionSnippet(
    @SerializedName("publishedAt")  val timestamp: String,
    @SerializedName("channelTitle") val channelName: String,
    @SerializedName("title")        val title: String,
    @SerializedName("description")  val description: String,
    @SerializedName("thumbnails")   val thumbnails: Thumbnails
)