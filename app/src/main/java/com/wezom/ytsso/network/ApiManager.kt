package com.wezom.ytsso.network

import com.wezom.ytsso.network.responses.RefreshedTokenResponse
import com.wezom.ytsso.network.responses.SubscriptionsResponse
import io.reactivex.Single
import io.reactivex.SingleTransformer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ApiManager(private val youtube: YoutubeApiService) {

    fun getSubscriptions(token: String): Single<SubscriptionsResponse> { // TODO: add token in interceptor
        return youtube.getSubscriptions("Bearer $token").compose(applySingleBgToFgSchedulers())
    }

    fun refreshToken(refreshToken: String): Single<RefreshedTokenResponse> {
        return youtube.refreshToken(refreshToken).compose(applySingleBgToFgSchedulers())
    }

    private fun <T> applySingleBgToFgSchedulers(): SingleTransformer<T, T> = SingleTransformer {
        it.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }
}