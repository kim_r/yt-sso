package com.wezom.ytsso.network.models

data class Subscription(
    val snippet: SubscriptionSnippet
)